package com.kutylo.model;

public interface Pizza {
    void prepare();

    void bake();

    void cut();

    void box();
}
