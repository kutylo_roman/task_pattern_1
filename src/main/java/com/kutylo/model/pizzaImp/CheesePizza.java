package com.kutylo.model.pizzaImp;

import com.kutylo.model.Pizza;

public class CheesePizza implements Pizza {
    @Override
    public void prepare() {

    }

    @Override
    public void bake() {

    }

    @Override
    public void cut() {

    }

    @Override
    public void box() {

    }
}
