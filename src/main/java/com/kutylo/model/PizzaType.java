package com.kutylo.model;

public enum PizzaType {
    CHEESE, VEGGIE, CLAM, PEPPERONI
}
